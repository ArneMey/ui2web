import 'bootstrap' // importeer bootstrap JavaScript code
import * as restClient from './js/restclient.js';
import { Monument } from './js/Monument.js';
import { initSearch } from './js/search.js'
import 'bootstrap/dist/css/bootstrap.css' // importeer bootstrap CSS code
import './css/style.scss'
import initHome from './js/home.js';
import initDetailButtons from './js/detail.js';

/* Eerste record standaard in formulier
restClient.getMonument(1).then(function(result) {
    pageController.putMonumentInForm(result)
})
*/

initDetailButtons();
initSearch();
initHome();