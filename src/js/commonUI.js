export function loadPage (page) {
    var pages = document.getElementsByName('page');
    for (var i = 0; i < pages.length; i++) {
        var elem = document.getElementById(pages[i].id);
        if (elem.id.includes(page))
            elem.classList.add('active');
        else
            elem.classList.remove('active');
      }
    }