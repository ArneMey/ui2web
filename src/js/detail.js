import { loadPage } from "./commonUI.js";
import { Monument } from "./Monument.js";
import * as restClient from './restclient.js';

export function putMonumentInForm (monument) {
  loadPage("detail")
  showDetailExisting();
  document.querySelector('#id').value = monument.id;
  document.querySelector('#name').value = monument.name;
  document.querySelector('#height').value = monument.height;
  document.querySelector('#constructionYear').value = monument.constructionYear;
  document.querySelector('#country').value = monument.country;
  document.querySelector('#image').src = monument.image;
}


export default function initDetailButtons () {
  document.getElementById('post').addEventListener('click', function (event) {
      let name = document.querySelector('#name').value;
      let height = document.querySelector('#height').value;
      let constructionYear = document.querySelector('#constructionYear').value;
      let country = document.querySelector('#country').value;
      let url = document.querySelector('#url').value;

      restClient
          .postMonument(new Monument(name, height, constructionYear, country, url))
          .then(function (responseData) {
              // response bevat de geposte person (), hieruit kan je de toegewezen id lezen
              document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Toevoegen gelukt.</div>';
              putMonumentInForm(responseData);
          })
          .catch(function (e) {
              console.log(e);
              showDetailExisting();
              document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Toevoegen mislukt.</div>';
          });
  });

  document.getElementById('put').addEventListener('click', async function () {
      let name = document.querySelector('#name').value;
      let height = document.querySelector('#height').value;
      let constructionYear = document.querySelector('#constructionYear').value;
      let country = document.querySelector('#country').value;
      let url = document.querySelector('#url').value;

      const monument = new Monument(name, height, constructionYear, country, url)
      monument.id = document.getElementById('id').value
      await restClient.putMomument(monument).then(function (responseData) {
        // response bevat de geposte person (), hieruit kan je de toegewezen id lezen
        document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Aanpassen gelukt.</div>';
    })
    })

  document.getElementById('delete').addEventListener('click', async function () {
      await restClient.deleteMonument(document.getElementById('id').value).then(function (responseData) {
        // response bevat de geposte person (), hieruit kan je de toegewezen id lezen
        document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Successvol verwijderd.</div>';
        loadPage("home");
    })
  })

  var labels = document.getElementsByName('detailPageLabel');
  for (var i = 0; i < labels.length; i++) {
      if (labels[i].htmlFor != '') {
           var elem = document.getElementById(labels[i].htmlFor);
           elem.addEventListener('change', function(){
            showDetailPage();
          });
      }
  }
  showDetailPage();
}

export function showDetailPage() {
    document.getElementById("detailsForm").reset(); 
   var labels = document.getElementsByName('detailPageLabel');
    for (var i = 0; i < labels.length; i++) {
        if (labels[i].htmlFor != '') {
          var elem = document.getElementById(labels[i].htmlFor);
           var label = document.querySelector('label#' + elem.id + '_label');
           if (elem.checked)
               label.classList.add('active');
           else
               label.classList.remove('active');
    }
  }

}

export function showDetailExisting() {
  document.getElementById('monument_existing').checked = true;
  showDetailPage();
}