import { initSearch } from "./search.js";

const MONUMENTS_URL = 'http://localhost:3000/monuments';
export const IMAGE_URL = 'http://localhost:3000/'


export async function postMonument (monument) {
  return await fetch(MONUMENTS_URL, {
    method: 'POST',
    headers: {
        // belangrijk! Anders weet de restserver niet wat voor data binnenkomt.Kan 400 Bad Request teruggeven of de request fout afhandelen
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(monument) // We zetten het JavaScript object om naar een JSON string en plaatsen die in de body van de HTTP Request
})
    .then(function (response) {
        if (!response.ok) {
            // als er een antwoord komt wordt de post als gelukt beschouwd (zelfs wanneer het antwoord 404 is bv)
            // we moeten dan dus checken op de response.ok
            throw Error(
                'Unable to POST the monument: ' +
        response.status +
        ' ' +
        response.statusText
            );
        }
        initSearch();
        return response.json();
    })
    .catch(function (e) {
        document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Fout bij aanmaken van monument.</div>';
        console.log(response.status)
    });
}

export async function getMonument (id) {
  return await fetch(MONUMENTS_URL + '/' + id).then(function (response) {
    if (!response.ok) {
      throw Error(
        'Unable to GET the brewery: ' +
          response.status +
          ' ' +
          response.statusText
      )
    }
    return response.json()
  })
  .catch(function (e) {
    document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Fout bij ophalen van data.</div>';
    console.log(response.status)
  });
}

export async function getMonuments () {
  return await fetch(MONUMENTS_URL)
      .then(function (response) {
          if (!response.ok) {
              throw Error(
                  'Unable to GET the monuments: ' +
          response.status +
          ' ' +
          response.statusText
              );
          }
          return response.json();
      })
      .catch(function (e) {
        document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Fout bij ophalen van data.</div>';
        console.log(response.status)
      });
}


export async function putMomument (monument) {
    return await fetch(MONUMENTS_URL + '/' + monument.id, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(monument)
    })
      .then(function (response) {
        if (!response.ok) {
          throw Error(
            'Unable to PUT the monument: ' +
      response.status +
      ' ' +
      response.statusText
          )
        }
        initSearch();
        return response.json()
      })
      .catch(function (e) {
        document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Fout bij aanpassen een monument.</div>';
        console.log(response.status)
      })
  }
  
  export async function deleteMonument (id) {
    try {
    const result = await fetch(MONUMENTS_URL + '/' + id, {
      method: 'DELETE'
    });
        if (!result.ok) {
          throw Error(
            'Unable to DELETE the monument: ' +
            result.status +
            ' ' +
            result.statusText
          )
        }
        initSearch();
        return await result.json()
  
    }
    catch (error) {
        document.querySelector('#feedback').innerHTML = '<div class="alert-info alert">Fout bij verwijderen van data.</div>';
        console.log(response.status)
      }
    }
  