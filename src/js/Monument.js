export class Monument {
    constructor (
        name,
        height,
        constructionYear,
        country,
        image,
        id=null
    ) {
        this.id=id;
        this.name = name;
        this.height = height;
        this.constructionYear = constructionYear;
        this.country = country;
        this.image = image;
    }
}
