import { putMonumentInForm } from './detail.js';
import { Monument } from './Monument.js';
import { getMonuments } from './restclient.js'

const sbar = document.getElementById('searchbar')
const stable = document.getElementById('searchtable')
let counter = 0

export async function initSearch () {
  let monuments = []
  sbar.addEventListener('input', function () {
    clearTable()
    const filtered = monuments.filter(monument => monument.name.toLowerCase().includes(sbar.value.toLowerCase()))
    show(filtered)
  })

  const data = await getMonuments()
  monuments = data.map(element => new Monument(element.name, element.height, element.constructionYear, element.country, element.image, element.id))
  for (let i = 0; i < data.length; i++) {
    const monument = new Monument(
      data[i].name,
      data[i].height,
      data[i].constructionYear,
      data[i].country,
      data[i].image,
      data[i].id
    )
    monuments[i] = monument
  }
  show(monuments)
}

export function show (toShow) {
  for (let i = 0; i < toShow.length; i++) {
    const row = stable.insertRow(i + 1)

    const id = row.insertCell(0)
    const name = row.insertCell(1)
    const height = row.insertCell(2)
    const constructionYear = row.insertCell(3)
    const country = row.insertCell(4)

    id.innerHTML = toShow[i].id
    name.innerHTML = toShow[i].name
    height.innerHTML = toShow[i].height
    constructionYear.innerHTML = toShow[i].constructionYear
    country.innerHTML = toShow[i].country

    row.classList.add('tablerow')

    row.addEventListener('click', async function () {
      putMonumentInForm(toShow[i])
    })

    counter++
  }
}

export function clearTable () {
  if (counter > 0) {
    for (let i = counter; i > 0; i--) {
      stable.deleteRow(i)
    }
  }
  counter = 0
}