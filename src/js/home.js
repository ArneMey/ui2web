import { putMonumentInForm } from "./detail.js"
import { getMonument } from "./restclient.js"

const images = document.getElementsByName('homeimage')
const names = document.getElementsByName('homename')
const cards = document.getElementsByName('homecart')

export default async function initHome () {
  for (let index = 0; index < images.length; index++) {
    const brewery = await getMonument(index + 1)
    images[index].src = brewery.image
    names[index].innerHTML = brewery.name
    cards[index].addEventListener('click', async function () {
      putMonumentInForm(brewery)
    })
  }
}